# twit

> Say nonsense! It's fun!

This is a demo application for django.

You will need to create a file called `.env` in the root directory with this in it:
```
SECRET_KEY='asecretkey'
DEBUG=True
ALLOWED_HOSTS=localhost
```

This project uses a virtual environment. You'll need to start it with:
```
source env/bin/activate
```







to quit virtual env:
```
deactivate
```
