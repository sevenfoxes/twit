from django.contrib import admin
from django.urls import include, path
from rest_framework import routers
from twit import views

router = routers.DefaultRouter()
router.register(r'tweeps', views.TweepViewSet)

urlpatterns = [
    path('', include('twit.urls')),
    path('api/', include(router.urls)),
    path('admin/', admin.site.urls),
]
