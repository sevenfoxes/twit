from rest_framework import serializers
from twit.models import Tweep

class TweepSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = Tweep
        fields = ('tweep_author', 'tweep_body', 'tweep_likes', 'tweep_date')
