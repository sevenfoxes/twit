from django.urls import path
from . import views

app_name = 'twit'
urlpatterns = [
    path('', views.index, name='index'),
]
