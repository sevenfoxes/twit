from django.http import HttpResponseRedirect
from django.shortcuts import get_object_or_404, render
from django.urls import reverse
from django.views.generic import TemplateView
from django.views.decorators.cache import never_cache
from rest_framework import viewsets
from twitcom.serializers import TweepSerializer
from rest_framework import status, viewsets
from rest_framework.response import Response
import logging

from .models import Tweep

index = never_cache(TemplateView.as_view(template_name='index.html'))

class TweepViewSet(viewsets.ModelViewSet):
    queryset = Tweep.objects.order_by('-tweep_date')
    serializer_class = TweepSerializer
