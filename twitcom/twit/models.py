from django.db import models

class Tweep(models.Model):
    tweep_author = models.CharField(max_length=40)
    tweep_body = models.CharField(max_length=50)
    tweep_likes = models.PositiveIntegerField(default=0)
    tweep_date = models.DateTimeField('date tweeped')

    def __str__(self):
        return self.tweep_body

    def get_absolute_url(self):
        return reverse("Tweep_detail", kwargs={"pk": self.pk})

class Comment(models.Model):
    tweep = models.ForeignKey(Tweep, on_delete=models.CASCADE, default=None)
    comment_text = models.CharField(max_length=50)
    comment_likes = models.PositiveIntegerField(default=0)
